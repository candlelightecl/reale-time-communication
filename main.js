const media_devices_contraints = {'video':true, 'audio':true};
const screen_cast_contraints = {vide:{cursor:'always', displaySurface:'window'}}

const openMediaDevices = async ( constraints ) => {
    return await navigator.mediaDevices.getUserMedia( constraints );
}

const openCamera = async (cameraId, minWidth, minHeight) => {
    const constraints = {
        'audio': {echoCancellation: false}, 
        'video':{
            'deviceId': cameraId,
            'width': {'min': minWidth},
            'height': {'min': minHeight}
        }
    }

    return await navigator.mediaDevices.getUserMedia( constraints );
}

const screenCast = async (constraints) => {
    return await navigator.mediaDevices.getDisplayMedia(constraints);
}

const availableMedia = async ( type ) => {
    const devices = await navigator.mediaDevices.enumerateDevices();
    return devices.filter( device => device.kind === type );
}

function bringMediaDevices(){
    try{
        const stream = openMediaDevices(media_devices_contraints)
    }catch( error ){
        console.error('Error accessing media services.', error);
    }
}

async function listMediaDevices() {
    try{
        const mediaFound = await availableMedia('videoinput')
        updateCameraList(mediaFound);

    }catch( error ){
        console.error('Error accessing media services.', error);
    }
}

function updateCameraList( cameras ){
    const listElement = document.querySelector("select#videoinputlist");
    listElement.innerHTML = '';
    cameras.map(camera => {
            const cameraOption = document.createElement('option');
            cameraOption.text = camera.kind + ' - ' + camera.label ;
            cameraOption.value = camera.deviceId;
            return cameraOption;
        }
    ).forEach( cameraOption => {
        listElement.add( cameraOption );
    });
}

navigator.mediaDevices.addEventListener('devicechange', async event => {
    const newCameraList = await availableMedia('videoinput');
    updateCameraList(newCameraList);
});

async function setUpCamera(){
    const camera = await availableMedia('videoinput');
    if(camera && camera.length > 0 ){
        try {
            const stream = await openCamera(camera[0].deviceId, 1280, 720);
            const videoElement = document.querySelector('video#localvideo');
            videoElement.srcObject = stream;
        } catch (error) {
            console.error('Error accessing media services.', error);
        }
      
    }
}

async function startScreenCast(){
    const screencast_stream = await screenCast(screen_cast_contraints);
    const videoElement = document.querySelector('video#localvideo');
    videoElement.srcObject = screencast_stream;
}